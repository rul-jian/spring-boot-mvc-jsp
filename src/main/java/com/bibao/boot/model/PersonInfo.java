package com.bibao.boot.model;

import java.util.List;

public class PersonInfo {
	private String message;
	private List<Person> personList;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Person> getPersonList() {
		return personList;
	}
	public void setPersonList(List<Person> personList) {
		this.personList = personList;
	}
}
