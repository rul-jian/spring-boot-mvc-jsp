package com.bibao.boot.springbootmvcjsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.bibao.boot")
public class SpringBootMvcJspApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMvcJspApplication.class, args);
	}
}
